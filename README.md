# nollytrend-app-prototype
A movie review app for nollywood movies. To promote Nigerian movies and make it more global.

## App Distribution Link 
- https://appdistribution.firebase.dev/i/c19b796d29caa2e1

## App Screenshots
| Screenshots  | Screenshots  | Screenshots |
| :------------ |:---------------:| -----:|
| ![home-screen](/uploads/cc5fbadaa7b46b6032a01a14f20559d0/home-screen.png)      | ![more-screen](/uploads/a2b2367b4a3988a64165f64f79265d22/more-screen.png) | ![launch-screen](/uploads/c81ed29ff27dae8c91b31deba93b204c/launch-screen.png) |
| ![series-screen](/uploads/f56e99700717b16fe01602d999c54b29/series-screen.png)      |         |    |

